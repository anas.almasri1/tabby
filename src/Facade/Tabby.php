<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is facade class for Helper packages
 */


namespace aqsat_integration_bnpl\tabby\Facade;


use Illuminate\Support\Facades\Facade;

/**
 * @method static \aqsat_integration_bnpl\tabby\api\Tabby tabby(string $debug = false, int $time_out = 30)
 */

class Tabby extends Facade{

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: Get the registered name of the component.
     *
     ***************************************************************
     */
    protected static function getFacadeAccessor(){

        return 'Tabby';
    }
}
