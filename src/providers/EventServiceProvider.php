<?php

namespace aqsat_integration_bnpl\tabby\providers;


use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {


    protected $listen = [];

    public function boot(){

        parent::boot();
    }
}
