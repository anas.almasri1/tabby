<?php

namespace aqsat_integration_bnpl\tabby\providers;

use aqsat_integration_bnpl\tabby\api\Main;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider{

    public function register() {

        if($this->app->runningInConsole()){

            $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

            $this->commands([  ]);
        }


        $this->registerConfig();

    }


    public function boot() {

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'tabby');

    }

    protected function registerConfig(){

        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('Tabby.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'tabby'
        );
        $this->app->singleton('Tabby', static function () {

            return new Main();
        });
    }
}
