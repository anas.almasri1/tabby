<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 */

namespace aqsat_integration_bnpl\tabby\api;

class Main{


    final public static function tabby(): Tabby
    {

        return Tabby::object(false);

    }

}
