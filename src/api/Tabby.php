<?php

namespace aqsat_integration_bnpl\tabby\api;

use Psr\Http\Message\ResponseInterface;

class Tabby{


    private array $endpoint = [

        'tabby'=>'tabby',
    ];


    private mixed $timeOut;


    private mixed $debug;


    final public function __construct($debug = false , $timeOut = 30){

        $this->debug = $debug ;

        $this->timeOut = $timeOut;
    }


    final public function offer()
    {


   $tabbyOffer =  [
    [       'BNPL_Name' => 'tabby',
            'Amount_Month' => '401',
            'Number_of_Installments' => '3',
            'Profit_Ratio' => '3',
            'Start_Date' => '2021-10-15',
            'End_Date' => '2022-01-15',
        'offer_id' => '1',

    ],
       [    'BNPL_Name' => 'tabby',
           'Amount_Month' => '201',
           'Number_of_Installments' => '4',
           'Profit_Ratio' => '6',
           'Start_Date' => '2021-02-15',
           'End_Date' => '2022-01-15',
           'offer_id' => '2',

       ],
       [
           'BNPL_Name' => 'tabby',
           'Amount_Month' => '101',
           'Number_of_Installments' => '5',
           'Profit_Ratio' => '9',
           'Start_Date' => '2020-10-15',
           'End_Date' => '2022-01-15',
           'offer_id' => '3',

       ]
];

        return $tabbyOffer;

        $parameter = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'x-api-token' => config('tabby.x_api_token')
            ],
            'json' => ['Data']
        ];

        return $this->Api()->setEndpoint($this->endpoint['tabby'])->setOptions($parameter)->get();
    }


    final public function paymentOffer()
    {

        $tabbyOffer =  [
            'BNPL_Name' => 'tabby',
            'Amount_Month' => '401',
            'Number_of_Installments' => '1',
            'Profit_Ratio' => '51',
            'Start_Date' => '2020-10-15',
            'End_Date' => '2022-01-15',
        ];

        return $tabbyOffer;


        $parameter = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'x-api-token' => config('tabby.x_api_token')
            ],
            'json' => ['Data']
        ];

        return $this->Api()->setEndpoint($this->endpoint['payment'])->setOptions($parameter)->get();
    }

    private function Api(){

        $base_url = 'https://bnpls.free.beeceptor.com/'; //for example payments API

        return new BaseGateway( $base_url , $this->debug , $this->timeOut );
    }


    final public static function object($debug = true , $timeOut = 30){

        return new self( $debug , $timeOut );
    }
}
