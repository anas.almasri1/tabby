<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This Based class for helper apis
 */

namespace aqsat_integration_bnpl\tabby\api;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class BaseGateway {

    private Client $client;

    private string $endPoint = '';

    protected array $header = [] ;

    protected array $options= [] ;

    public function __construct(string $baseUri,bool $debug = false, int $timeOut = 30 ) {

        $this->client = new Client( [

            'base_uri'        =>    $baseUri,

            'headers'         =>    $this->getHeader(),

            'debug'           =>    $debug,

            'timeout'         =>    $timeOut,

        ] ) ;
    }

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: This set endpoint for http request
     * @param string $endPoint
     * @return $this
     *
     ***************************************************************
     */
    final public function setEndPoint(string $endPoint): static
    {

        $this->endPoint = $endPoint ;

        return $this;
    }

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: This get endpoint for http request
     * @return string
     *
     ***************************************************************
     */
    private function getEndPoint() : string {

        return $this->endPoint;
    }


    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: This getHeader for http request
     * @return array
     *
     ***************************************************************
     */
    private function getHeader() : array {

        return  $this->header;
    }

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: This get Client request
     * @return Client
     *
     ***************************************************************
     */
    private function getClient(): Client {

        return $this->client;
    }


    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: This set api options for http request
     * @param array $options
     * @return $this
     *
     ***************************************************************
     */
    final public function setOptions( array $options ): static
    {

        $this->options = $options ;

        return $this;
    }


    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: This get options for http request
     * @return array
     *
     ***************************************************************
     */
    private function getOptions() : array {

        return  $this->options;
    }


    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: This send get request and get data as response Interface
     * @return ResponseInterface
     *
     ***************************************************************
     */
    public function get() : ResponseInterface{

        return  $this->getClient()->get( $this->getEndPoint() , $this->getOptions() );
    }

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: This send get request and post data as response Interface
     * @return ResponseInterface
     *
     ***************************************************************
     */
    public function post() : ResponseInterface{

        return  $this->getClient()->post( $this->getEndPoint() , $this->getOptions() );
    }



}
