<?php

namespace aqsat_integration_bnpl\tabby\Foundation;

class Validation {

    final static public function bnplForm() {

        $data = [];

        $data['user_name'] = [
            'validation' => 'required',
            'label' => 'User name ID',
            'name' => 'user_name',
            'order' => 1,
            'type' => 'text' //todo add as constant
            ];

        $data['key'] = [
            'validation' => 'required',
            'label' => 'key',
            'name' => 'key',
            'order' => 2,
            'type' => 'text' //todo add as constant
        ];


     return $data;

    }

    final static public function bnplFormValidation(){

        $rules['user_name'] = ['required'];

        $rules['key'] = ['required'];

        return $rules;

    }

}
