<?php

namespace aqsat_integration_bnpl\tabby\Foundation;

use aqsat\bnpl\Interface\BnplInterface;
use aqsat\orders\Constants\OrderStatus;
use GuzzleHttp\Exception\ClientException;
use aqsat_integration_bnpl\tabby\Facade\Tabby;



class ProviderBnpl implements BnplInterface{


    public function getOffers(){

        try {

            $response = Tabby::tabby()->offer();

            //$response =  json_decode($response->getBody()->getContents() , true);

            sleep(1);

            return $response;

        }catch (ClientException $exception){

            return $exception->getMessage(); // todo add logs

        }
    }

    public function bnplForm(){

        return Validation::bnplForm();

    }

    public function bnplFormValidation(){

        return Validation::bnplFormValidation();

    }

     public function payment()
    {
        sleep(1);

        return [
            'status'=> OrderStatus::COMPLETED,
            'messages'=>'Payment Success',
            'amount'=>401
        ];

        try {
            $response = Tabby::tabby()->paymentOffer();

            //$response =  json_decode($response->getBody()->getContents() , true);
            sleep(1);

            return $response;

        }catch (ClientException $exception){

            return $exception->getMessage(); // todo add logs

        }
    }


    public function agreementPage(){
        return trans('tabby::response.agreement_page');

    }
}
